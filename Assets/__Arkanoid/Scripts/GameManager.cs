﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    int m_numberOfLevels;
    int m_currentLevel = 1;

    // Start is called before the first frame update
    void Start()
    {
        Block.OnAllBlocksDestroyed += ChangeToNextScene;
        
        m_numberOfLevels = SceneManager.sceneCount - 1; // Restar la escena BaseGame
        UnloadAllScenesButFirst();
    }

    void OnDestroy()
    {
        Block.OnAllBlocksDestroyed -= ChangeToNextScene;
    }

    void UnloadAllScenesButFirst()
    {
        for (int i = 2; i <= m_numberOfLevels; i++)
        {
            SceneManager.UnloadSceneAsync("Level_" + i);
        }
    }

    void ChangeToNextScene()
    {
        for (int i = 1; i <= m_numberOfLevels; i++)
        {
            if (m_currentLevel == m_numberOfLevels)
            {
                Debug.Log("game finished");
                return;
            }

            if (m_currentLevel == i)
            {
                SceneManager.UnloadSceneAsync("Level_" + i);
                m_currentLevel++;
                SceneManager.LoadSceneAsync("Level_" + m_currentLevel, LoadSceneMode.Additive);
                return;
            }
        }
    }
}
