﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speed_ball_Effect : MonoBehaviour
{
    [Range(-25, 25)]
    [SerializeField] int m_percentOfSpeedBallChanger;
    BoxCollider2D m_collider;

    public delegate void SpeedBallEffect(int percentOfSpeedBallChanger);
    public static event SpeedBallEffect OnSpeedBallEffect;

    // Start is called before the first frame update
    void Awake()
    {
        m_collider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckColliders();
    }

    private void CheckColliders()
    {
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, m_collider.size, 0);

        foreach (Collider2D hit in hits)
        {

            if (hit == m_collider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(m_collider);


            if (colliderDistance.isOverlapped)
            {
                IfHitPaddle(hit, colliderDistance);
            }
        }
    }

    void IfHitPaddle(Collider2D hit, ColliderDistance2D colliderDistance)
    {
        if (hit.gameObject.CompareTag("Paddle"))
        {
            // hacer efecto
            if (OnSpeedBallEffect != null) OnSpeedBallEffect(m_percentOfSpeedBallChanger);
            Debug.Log("m_percentOfSpeedBallChanger: " + m_percentOfSpeedBallChanger);
            Destroy(gameObject);
        }
    }
}
