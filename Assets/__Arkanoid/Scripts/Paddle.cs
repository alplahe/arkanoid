﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    [SerializeField] float m_speed = 5.0f;
    float m_currentSpeed;
    Vector3 m_newPosition;

    float m_halfColliderWidth;

    BoxCollider2D m_collider;

    void Awake()
    {
        m_newPosition = transform.position;
        m_collider = GetComponent<BoxCollider2D>();
        m_halfColliderWidth = m_collider.size.x / 2.0f;
    }

    public float GetHalfColliderPaddleWith()
    {
        return m_halfColliderWidth;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdateVelocity();
        UpdatePosition();
    }

    void UpdateVelocity()
    {
        if (Input.GetButton("MoveRight") && !IsWallHit())
        {
            m_currentSpeed = m_speed;
        }
        else if (Input.GetButton("MoveLeft") && !IsWallHit())
        {
            m_currentSpeed = m_speed * -1.0f;
        }
        else
        {
            m_currentSpeed = 0.0f;
        }
    }

    void UpdatePosition()
    {
        transform.Translate(new Vector3(m_currentSpeed * Time.deltaTime, 0, 0));
        m_newPosition.x = transform.position.x;
        m_newPosition.y = transform.position.y;
        m_newPosition.z = 0;

        transform.position = m_newPosition;
    }

    bool IsWallHit()
    {
        if (CheckColliders())
        {
            return true;
        }
        return false;
    }

    bool CheckColliders()
    {
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, m_collider.size, 0);

        foreach (Collider2D hit in hits)
        {
            if (hit == m_collider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(m_collider);

            if (colliderDistance.isOverlapped)
            {
                if (IfHitScene(hit))
                {
                    RepositionateBall(colliderDistance);
                    return true;
                }
            }
        }
        return false;
    }

    bool IfHitScene(Collider2D hit)
    {
        if (hit.gameObject.CompareTag("Scene"))
        {
            return true;
        }
        return false;
    }

    void RepositionateBall(ColliderDistance2D colliderDistance)
    {
        transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
    }
}
