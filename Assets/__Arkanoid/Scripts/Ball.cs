﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour, IHealth
{
    [SerializeField] float m_speed = 3.0f;
    Vector3 m_newPosition;
    Vector3 m_startingPosition;
    Vector3 m_direction;
    [SerializeField] float m_startingAngle = -90.0f;

    BoxCollider2D m_collider;

    [SerializeField] int m_startingLifes = 3;
    int m_leftLifes;
    public event Action<int> CurrentBalls = delegate { };
    public event Action OnGameOver = delegate { };

    const float m_BounceAngleHalfRange = 60.0f * Mathf.Deg2Rad;

    [SerializeField] List<GameObject> m_goodCapsules;
    [SerializeField] List<GameObject> m_badCapsules;
    [Range(0,50)]
    [SerializeField] int m_percentOfGoodCapsule;
    [Range(0, 50)]
    [SerializeField] int m_percentOfBadCapsule;

    void Awake()
    {
        m_newPosition = transform.position;
        m_startingPosition = transform.position;
        
        // Calculate direction vector with angle in radians
        m_direction = new Vector3(Mathf.Cos(m_startingAngle * Mathf.Deg2Rad), Mathf.Sin(m_startingAngle * Mathf.Deg2Rad), 0);

        m_collider = GetComponent<BoxCollider2D>();

        m_leftLifes = m_startingLifes;
    }

    // Start is called before the first frame update
    void Start()
    {
        CurrentBalls(m_leftLifes);

        Speed_ball_Effect.OnSpeedBallEffect += HandleSpeedBallEffect;
    }

    void HandleSpeedBallEffect(int percentOfSpeedBallChanger)
    {
        m_speed *= 1f + (float)percentOfSpeedBallChanger / 100f;
    }

    void OnDestroy()
    {
        Speed_ball_Effect.OnSpeedBallEffect -= HandleSpeedBallEffect;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdatePosition();
        CheckColliders();
    }

    void UpdatePosition()
    {
        transform.Translate(new Vector3(m_speed * m_direction.x * Time.deltaTime, m_speed * m_direction.y * Time.deltaTime, 0));
        m_newPosition.x = transform.position.x;
        m_newPosition.y = transform.position.y;
        m_newPosition.z = 0;

        transform.position = m_newPosition;
    }

    private void CheckColliders()
    {
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, m_collider.size, 0);

        foreach (Collider2D hit in hits)
        {

            if (hit == m_collider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(m_collider);


            if (colliderDistance.isOverlapped)
            {
                CheckUpperCollisions(colliderDistance, hit);
                CheckLowerCollisions(colliderDistance, hit);
                CheckLeftCollisions(colliderDistance, hit);
                CheckRightCollisions(colliderDistance, hit);
            }
        }
    }

    void CheckUpperCollisions(ColliderDistance2D colliderDistance, Collider2D hit)
    {
        if (Vector2.Angle(colliderDistance.normal, Vector2.down) < 90 && m_direction.y > 0)
        {
            RepositionateBall(colliderDistance);
            m_direction.y *= -1f;
            IfHitBlock(hit);
        }
    }

    void CheckLowerCollisions(ColliderDistance2D colliderDistance, Collider2D hit)
    {
        if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && m_direction.y < 0)
        {
            IfHitPaddle(hit, colliderDistance);
        }
    }

    void CheckLeftCollisions(ColliderDistance2D colliderDistance, Collider2D hit)
    {
        if (Vector2.Angle(colliderDistance.normal, Vector2.right) < 90 && m_direction.x < 0)
        {
            RepositionateBall(colliderDistance);
            m_direction.x *= -1f;
            IfHitBlock(hit);
        }
    }

    void CheckRightCollisions(ColliderDistance2D colliderDistance, Collider2D hit)
    {
        if (Vector2.Angle(colliderDistance.normal, Vector2.left) < 90 && m_direction.x > 0)
        {
            RepositionateBall(colliderDistance);
            m_direction.x *= -1f;
            IfHitBlock(hit);
        }
    }

    void IfHitPaddle(Collider2D hit, ColliderDistance2D colliderDistance)
    {
        if (hit.gameObject.CompareTag("Paddle"))
        {
            // calculate new ball direction
            float ballOffsetFromPaddleCenter = hit.transform.position.x - transform.position.x;
            float normalizedBallOffset = ballOffsetFromPaddleCenter / hit.GetComponent<Paddle>().GetHalfColliderPaddleWith();
            float angleOffset = normalizedBallOffset * m_BounceAngleHalfRange;
            float angle = Mathf.PI / 2 + angleOffset;
            Vector2 direction = new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
            m_direction.x = direction.x;
            m_direction.y = direction.y;
        }
        else
        {
            RepositionateBall(colliderDistance);
            m_direction.y *= -1f;

            IfHitScene(hit);
            IfHitBlock(hit);
        }
    }

    void RepositionateBall(ColliderDistance2D colliderDistance)
    {
        transform.Translate(colliderDistance.pointA - colliderDistance.pointB);
    }

    void IfHitScene(Collider2D hit)
    {
        if (hit.gameObject.CompareTag("Scene"))
        {
            LostBall();
        }
    }

    void LostBall()
    {
        ManageLifes();
        
        SetTheBallInTheStartingPosition();
    }
    void SetTheBallInTheStartingPosition()
    {
        transform.position = m_startingPosition;
        m_direction = new Vector3(Mathf.Cos(m_startingAngle * Mathf.Deg2Rad), Mathf.Sin(m_startingAngle * Mathf.Deg2Rad), 0);

        if (IsGameOver())
        {
            m_speed = 0;
        }
    }

    void ManageLifes()
    {
        m_leftLifes--;

        CurrentBalls(m_leftLifes);
    }

    bool IsGameOver()
    {
        if (m_leftLifes < 0)
        {
            Debug.Log("gameover");
            return true;
        }
        return false;
    }

    void IfHitBlock(Collider2D hit)
    {
        if (hit.gameObject.CompareTag("Block"))
        {
            SpawnCapsule();
            hit.gameObject.GetComponent<Block>().BlockHit();
        }
    }

    void SpawnCapsule()
    {
        int randomGoodCapsule = UnityEngine.Random.Range(0, 100);
        int randomBadCapsule = UnityEngine.Random.Range(0, 100);

        if(randomGoodCapsule <= m_percentOfGoodCapsule)
        {
            GameObject capsule = Instantiate(m_goodCapsules[0]); // ahora coge el indice 0, pero habria que implementar que elija al azar entre la lista
            capsule.transform.position = transform.position;
        }
        else if (randomBadCapsule <= m_percentOfBadCapsule)
        {
            GameObject capsule = Instantiate(m_badCapsules[0]);
            capsule.transform.position = transform.position;
        }
    }
}
