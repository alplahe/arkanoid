﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum BLOCK_TYPE { One_hit, Two_hit, Three_hit, Indestructible }
public class Block : MonoBehaviour
{
    [SerializeField] Sprite m_sprite;
    [SerializeField] BLOCK_TYPE m_blockType;
    int m_life;

    public delegate void AllBlocksDestroyed();
    public static event AllBlocksDestroyed OnAllBlocksDestroyed;

    void OnValidate()
    {
        GetComponent<SpriteRenderer>().sprite = m_sprite;
    }

    // Start is called before the first frame update
    void Start()
    {
        switch (m_blockType)
        {
            case BLOCK_TYPE.One_hit:
                m_life = 1;
                break;
            case BLOCK_TYPE.Two_hit:
                m_life = 2;
                break;
            case BLOCK_TYPE.Three_hit:
                m_life = 3;
                break;
            default:
                break;
        }
    }

    public void BlockHit()
    {
        if (m_blockType == BLOCK_TYPE.Indestructible)
            return;
        
        m_life--;

        if(m_life <= 0)
        {
            if (GetBlockAmountLeft() == 1)
            {
                Debug.Log("all blocks destroyed");

                //load another scene
                if (OnAllBlocksDestroyed != null) OnAllBlocksDestroyed();
            }

            Destroy(gameObject);
        }
    }

    int GetBlockAmountLeft()
    {
        return transform.parent.GetComponent<Transform>().childCount;
    }
}
