﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHealth
{
    event System.Action<int> CurrentBalls;
    event System.Action OnGameOver;
}
