﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Capsule_Movement : MonoBehaviour
{
    [SerializeField] float m_speed = 1.0f;
    Vector3 m_newPosition;
    Vector3 m_direction;
    float m_startingAngle = -90.0f;

    // Start is called before the first frame update
    void Awake()
    {
        m_newPosition = transform.position;

        // Calculate direction vector with angle in radians
        m_direction = new Vector3(Mathf.Cos(m_startingAngle * Mathf.Deg2Rad), Mathf.Sin(m_startingAngle * Mathf.Deg2Rad), 0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {
        transform.Translate(new Vector3(m_speed * m_direction.x * Time.deltaTime, m_speed * m_direction.y * Time.deltaTime, 0));
        m_newPosition.x = transform.position.x;
        m_newPosition.y = transform.position.y;
        m_newPosition.z = 0;

        transform.position = m_newPosition;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
