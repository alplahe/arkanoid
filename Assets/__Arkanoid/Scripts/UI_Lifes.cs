﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Lifes : MonoBehaviour
{
    Text m_textLifes;

    // Start is called before the first frame update
    void Awake()
    {
        GetComponentInParent<IHealth>().CurrentBalls += HandleCurrentBalls;
        m_textLifes = GetComponentInChildren<Text>();
    }
    
    void HandleCurrentBalls(int currentBalls)
    {
        m_textLifes.text = "Lifes: " + currentBalls.ToString();
    }
}
